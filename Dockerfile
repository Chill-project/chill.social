FROM node:9 AS build

WORKDIR /app

COPY . /app/

RUN npm install
RUN npm install -g bower
RUN npm install -g grunt-cli
RUN grunt install


FROM nginx:1.14-alpine

COPY --from=build /app/website /usr/share/nginx/html
COPY ./website/index.html  /usr/share/nginx/html/index.html

RUN { \
  echo ""; \
  echo "server {"; \
  echo "    root /usr/share/nginx/html;"; \
  echo "    index index.html;"; \
  echo "}"; \
  echo ""; \
} >> /etc/nginx/conf.d/default-add.conf
